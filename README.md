### rOathtool
---

A tool to generate TOTP/HOTP, written in Rust.

Usage:

```bash
> roauthtool --digits 6 --key ORSXG5AK --type totp --window 30
262961

> roauthtool --digits 6 --key ORSXG5AK --type hotp --window 30 --counter 100
550906

> roauthtool --help

roauthtool 0.1.0

USAGE:
    roauthtool [OPTIONS] --digits <digits> --key <key> --type <type>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --counter <counter>    required for HOTP [default: 1]
    -d, --digits <digits>       [possible values: 6, 7, 8]
    -k, --key <key>            base32 encoded string
    -t, --type <type>           [possible values: hotp, totp]
    -w, --window <window>       [default: 30]
```

Use `cargo build` to compile

_Currently, this is a very simple one file project, my attempt at learning Rust._

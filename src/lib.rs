use ring::hmac;
use chrono::UTC;
use rand::distributions::{Uniform, Distribution};

pub fn get_time_counter(window: u64) -> u64 {
    let ts = UTC::now().timestamp() as u64;
    ts / window
}

pub fn calculate_otp(_key: String, counter: u64, digits: i8) -> String {
    let alphabet = base32::Alphabet::RFC4648 { padding: true };
    let decoded_key = base32::decode(alphabet, &_key).unwrap();
    let key = hmac::Key::new(
        hmac::HMAC_SHA1_FOR_LEGACY_USE_ONLY,
        &decoded_key,
    );
    let msg = counter.to_be_bytes();
    let tag = hmac::sign(&key, &msg);
    let tag_bytes = tag.as_ref(); 

    let offset = (tag_bytes.last().unwrap() & 0xf) as usize;
    let mut result: i64 = ((tag_bytes[offset] & 0x7f) as i64) << 24;
    result |= ((tag_bytes[offset + 1] & 0xff) as i64) << 16;
    result |= ((tag_bytes[offset + 2] & 0xff) as i64) << 8;
    result |= (tag_bytes[offset + 3] & 0xff) as i64;
    let base: i64 = 10;
    (result % base.pow(digits as u32)).to_string()
}

pub fn secret() -> String {
    let series = String::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567");
    let upper = series.len();
    let distribution = Uniform::from(0..upper);
    let mut rng = rand::thread_rng();
    let mut result = String::from("");
    for _ in 0..16 {
        let index = distribution.sample(&mut rng);
        result.push(series.as_bytes()[index] as char);
    }
    return result;
}

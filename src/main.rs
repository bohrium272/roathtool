use std::str;
use structopt::StructOpt;
use clap::arg_enum;
use roauthtool::{calculate_otp, secret, get_time_counter};

arg_enum! {
	#[derive(Debug)]
	enum Command {
	    Hotp,
	    Totp,
            Secret,
	}
}

#[derive(StructOpt, Debug)]
struct Parameters {
    #[structopt(short, long, possible_values = &Command::variants())]
    _command: Command,

    #[structopt(short, long, default_value = "6", possible_values = &["6", "7", "8"])]
    digits: i8,

    #[structopt(long, default_value = "1", help = "required for HOTP, defaults to 1")]
    counter: u64,

    #[structopt(short, long, help = "base32 encoded string")]
    key: Option<String>,

    #[structopt(short, long, default_value = "30")]
    window: u64,
}

fn main() {
    let args = Parameters::from_args();
    let result = match args._command {
        Command::Secret => secret(),
        Command::Hotp => calculate_otp(args.key.unwrap(), args.counter, args.digits),
        Command::Totp => calculate_otp(args.key.unwrap(), get_time_counter(args.window), args.digits),
    };
    println!("{}", result);
}
